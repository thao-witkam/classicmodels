<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210202082507 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY orders_ibfk_1');
        $this->addSql('ALTER TABLE payments DROP FOREIGN KEY payments_ibfk_1');
        $this->addSql('ALTER TABLE customers DROP FOREIGN KEY customers_ibfk_1');
        $this->addSql('ALTER TABLE employees DROP FOREIGN KEY employees_ibfk_1');
        $this->addSql('ALTER TABLE employees DROP FOREIGN KEY employees_ibfk_2');
        $this->addSql('ALTER TABLE orderdetails DROP FOREIGN KEY orderdetails_ibfk_1');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, extern_id INT NOT NULL, password VARCHAR(255) DEFAULT NULL, currency VARCHAR(3) DEFAULT NULL, merchant_notes VARCHAR(255) NOT NULL, customer_name VARCHAR(255) NOT NULL, shipping_address VARCHAR(255) NOT NULL, shipping_method VARCHAR(255) NOT NULL, ship_on DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP TABLE customers');
        $this->addSql('DROP TABLE employees');
        $this->addSql('DROP TABLE offices');
        $this->addSql('DROP TABLE orderdetails');
        $this->addSql('DROP TABLE orders');
        $this->addSql('DROP TABLE payments');
        $this->addSql('ALTER TABLE productlines CHANGE productLine productLine VARCHAR(255) NOT NULL, CHANGE textDescription textDescription LONGTEXT NOT NULL, CHANGE htmlDescription htmlDescription VARCHAR(255) DEFAULT NULL, CHANGE image image VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE products CHANGE productCode productCode VARCHAR(50) NOT NULL, CHANGE productName productName VARCHAR(255) NOT NULL, CHANGE productLine productLine VARCHAR(255) NOT NULL, CHANGE productVendor productVendor VARCHAR(255) NOT NULL, CHANGE quantityInStock quantityInStock INT NOT NULL');
        $this->addSql('ALTER TABLE products RENAME INDEX productline TO IDX_B3BA5A5ADC62562D');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE customers (customerNumber INT NOT NULL, customerName VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, contactLastName VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, contactFirstName VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, phone VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, addressLine1 VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, addressLine2 VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, city VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, state VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, postalCode VARCHAR(15) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, country VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, salesRepEmployeeNumber INT DEFAULT NULL, creditLimit NUMERIC(10, 2) DEFAULT NULL, INDEX salesRepEmployeeNumber (salesRepEmployeeNumber), PRIMARY KEY(customerNumber)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE employees (employeeNumber INT NOT NULL, lastName VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, firstName VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, extension VARCHAR(10) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, email VARCHAR(100) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, officeCode VARCHAR(10) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, reportsTo INT DEFAULT NULL, jobTitle VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, INDEX officeCode (officeCode), INDEX reportsTo (reportsTo), PRIMARY KEY(employeeNumber)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE offices (officeCode VARCHAR(10) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, city VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, phone VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, addressLine1 VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, addressLine2 VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, state VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, country VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, postalCode VARCHAR(15) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, territory VARCHAR(10) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(officeCode)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE orderdetails (orderNumber INT NOT NULL, productCode VARCHAR(15) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, quantityOrdered INT NOT NULL, priceEach NUMERIC(10, 2) NOT NULL, orderLineNumber SMALLINT NOT NULL, INDEX productCode (productCode), INDEX IDX_489AFCDC989A8203 (orderNumber), PRIMARY KEY(orderNumber, productCode)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE orders (orderNumber INT NOT NULL, orderDate DATE NOT NULL, requiredDate DATE NOT NULL, shippedDate DATE DEFAULT NULL, status VARCHAR(15) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, comments TEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, customerNumber INT NOT NULL, INDEX customerNumber (customerNumber), PRIMARY KEY(orderNumber)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE payments (customerNumber INT NOT NULL, checkNumber VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, paymentDate DATE NOT NULL, amount NUMERIC(10, 2) NOT NULL, INDEX IDX_65D29B32D53183C5 (customerNumber), PRIMARY KEY(customerNumber, checkNumber)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE customers ADD CONSTRAINT customers_ibfk_1 FOREIGN KEY (salesRepEmployeeNumber) REFERENCES employees (employeeNumber)');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT employees_ibfk_1 FOREIGN KEY (reportsTo) REFERENCES employees (employeeNumber)');
        $this->addSql('ALTER TABLE employees ADD CONSTRAINT employees_ibfk_2 FOREIGN KEY (officeCode) REFERENCES offices (officeCode)');
        $this->addSql('ALTER TABLE orderdetails ADD CONSTRAINT orderdetails_ibfk_1 FOREIGN KEY (orderNumber) REFERENCES orders (orderNumber)');
        $this->addSql('ALTER TABLE orderdetails ADD CONSTRAINT orderdetails_ibfk_2 FOREIGN KEY (productCode) REFERENCES products (productCode)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT orders_ibfk_1 FOREIGN KEY (customerNumber) REFERENCES customers (customerNumber)');
        $this->addSql('ALTER TABLE payments ADD CONSTRAINT payments_ibfk_1 FOREIGN KEY (customerNumber) REFERENCES customers (customerNumber)');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('ALTER TABLE productlines CHANGE productLine productLine VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE textDescription textDescription VARCHAR(4000) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE htmlDescription htmlDescription MEDIUMTEXT CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE image image MEDIUMBLOB DEFAULT NULL');
        $this->addSql('ALTER TABLE products CHANGE productCode productCode VARCHAR(15) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE productName productName VARCHAR(70) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE productVendor productVendor VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, CHANGE quantityInStock quantityInStock SMALLINT NOT NULL, CHANGE productLine productLine VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`');
        $this->addSql('ALTER TABLE products RENAME INDEX idx_b3ba5a5adc62562d TO productLine');
    }
}
