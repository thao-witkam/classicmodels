<?php

namespace App\Entity;

use App\Repository\ProductLineRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass=ProductLineRepository::class)
 * @ORM\Table(name="productlines")
 */
class ProductLine
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255, name="productLine")
     */
    private $ProductLine;

    /**
     * @ORM\Column(type="text", name="textDescription")
     */
    private $TextDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="htmlDescription")
     */
    private $HtmlDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="image")
     */
    private $Image;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="ProductLine")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getProductLine(): ?string
    {
        return $this->ProductLine;
    }

    public function setProductLine(string $ProductLine): self
    {
        $this->ProductLine = $ProductLine;

        return $this;
    }

    public function getTextDescription(): ?string
    {
        return $this->TextDescription;
    }

    public function setTextDescription(string $TextDescription): self
    {
        $this->TextDescription = $TextDescription;

        return $this;
    }

    public function getHtmlDescription(): ?string
    {
        return $this->HtmlDescription;
    }

    public function setHtmlDescription(?string $HtmlDescription): self
    {
        $this->HtmlDescription = $HtmlDescription;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    public function setImage(?string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setProductLine($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getProductLine() === $this) {
                $product->setProductLine(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getProductLine();
    }
}
