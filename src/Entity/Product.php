<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"}
 * )
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(name="products")
 */
class Product
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=50, name="productCode")
     */
    private $ProductCode;

    /**
     * @ORM\Column(type="string", length=255, name="productName")
     */
    private $ProductName;

    /**
     * @ORM\ManyToOne(targetEntity=ProductLine::class, inversedBy="products")
     * @ORM\JoinColumn(name="productLine", referencedColumnName="productLine", nullable=false)
     */
    private $ProductLine;

    /**
     * @ORM\Column(type="string", length=10, name="productScale")
     */
    private $ProductScale;

    /**
     * @ORM\Column(type="string", length=255, name="productVendor")
     */
    private $ProductVendor;

    /**
     * @ORM\Column(type="text", name="productDescription")
     */
    private $ProductDescription;

    /**
     * @ORM\Column(type="integer", name="quantityInStock")
     */
    private $QuantityInStock;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, name="buyPrice")
     */
    private $BuyPrice;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, name="msrp")
     */
    private $MSRP;

    public function getProductCode(): ?string
    {
        return $this->ProductCode;
    }

    public function setProductCode(string $ProductCode): self
    {
        $this->ProductCode = $ProductCode;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->ProductName;
    }

    public function setProductName(string $ProductName): self
    {
        $this->ProductName = $ProductName;

        return $this;
    }

    public function getProductLine(): ?ProductLine
    {
        return $this->ProductLine;
    }

    public function setProductLine(?ProductLine $ProductLine): self
    {
        $this->ProductLine = $ProductLine;

        return $this;
    }

    public function getProductScale(): ?string
    {
        return $this->ProductScale;
    }

    public function setProductScale(string $ProductScale): self
    {
        $this->ProductScale = $ProductScale;

        return $this;
    }

    public function getProductVendor(): ?string
    {
        return $this->ProductVendor;
    }

    public function setProductVendor(string $ProductVendor): self
    {
        $this->ProductVendor = $ProductVendor;

        return $this;
    }

    public function getProductDescription(): ?string
    {
        return $this->ProductDescription;
    }

    public function setProductDescription(string $ProductDescription): self
    {
        $this->ProductDescription = $ProductDescription;

        return $this;
    }

    public function getQuantityInStock(): ?int
    {
        return $this->QuantityInStock;
    }

    public function setQuantityInStock(int $QuantityInStock): self
    {
        $this->QuantityInStock = $QuantityInStock;

        return $this;
    }

    public function getBuyPrice(): ?string
    {
        return $this->BuyPrice;
    }

    public function setBuyPrice(string $BuyPrice): self
    {
        $this->BuyPrice = $BuyPrice;

        return $this;
    }

    public function getMSRP(): ?string
    {
        return $this->MSRP;
    }

    public function setMSRP(string $MSRP): self
    {
        $this->MSRP = $MSRP;

        return $this;
    }
}
