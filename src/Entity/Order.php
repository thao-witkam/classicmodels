<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "get",
 *      "post"={"access_control"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"}
 *      },
 *     itemOperations={
 *      "get",
 *     "put"
 *     }
 * )
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $extern_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $merchant_notes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $customer_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shipping_address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shipping_method;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ship_on;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExternId(): ?string
    {
        return $this->extern_id;
    }

    public function setExternId(string $extern_id): self
    {
        $this->extern_id = $extern_id;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getMerchantNotes(): ?string
    {
        return $this->merchant_notes;
    }

    public function setMerchantNotes(string $merchant_notes): self
    {
        $this->merchant_notes = $merchant_notes;

        return $this;
    }

    public function getCustomerName(): ?string
    {
        return $this->customer_name;
    }

    public function setCustomerName(string $customer_name): self
    {
        $this->customer_name = $customer_name;

        return $this;
    }

    public function getShippingAddress(): ?string
    {
        return $this->shipping_address;
    }

    public function setShippingAddress(string $shipping_address): self
    {
        $this->shipping_address = $shipping_address;

        return $this;
    }

    public function getShippingMethod(): ?string
    {
        return $this->shipping_method;
    }

    public function setShippingMethod(string $shipping_method): self
    {
        $this->shipping_method = $shipping_method;

        return $this;
    }

    public function getShipOn(): ?\DateTimeInterface
    {
        return $this->ship_on;
    }

    public function setShipOn(?\DateTimeInterface $ship_on): self
    {
        $this->ship_on = $ship_on;

        return $this;
    }
}
